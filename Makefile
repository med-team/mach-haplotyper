# 
# MaCH Makefile -- Compiles and installs MACH and accessory applications 
# (c) 2006-2007 Goncalo Abecasis
#

# current version of the software 
VERSION=1.0.18.c

# default installation directory 
INSTALLDIR=/usr/local/bin

# default C++ compiler 
CXX=g++

# default compilation flags are 
# 
# CFLAGS=-O2 -I./libsrc/ -I./mach1 
# 
CFLAGS=-O2 -static -I./libsrc -I./mach1 -D__ZLIB_AVAILABLE__  -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE

# executable file names and locations 
BINDIR = executables
MACH = $(BINDIR)/mach1
THUNDER = $(BINDIR)/thunder
EXECUTABLES = $(MACH) $(THUNDER)

# MACH File Set 
MACHBASE = mach1/AssociationAnalysis mach1/CostCalculator \
 mach1/DosageCalculator mach1/ErrorRate mach1/Manners \
 mach1/Haplotyper mach1/HaplotypeKey mach1/HaplotypeLoader \
 mach1/MergeHaplotypes mach1/OutputHandlers
MACHHDR = $(MACHBASE:=.h)
MACHSRC = $(MACHBASE:=.cpp) mach1/Main.cpp
MACHOBJ = $(MACHSRC:.cpp=.o)

# THUNDER File Set
THUNDERBASE = mach1/AssociationAnalysis mach1/CostCalculator \
 mach1/DosageCalculator mach1/ErrorRate \
 mach1/Haplotyper mach1/HaplotypeKey mach1/HaplotypeLoader \
 mach1/MergeHaplotypes mach1/OutputHandlers \
 thunder/ShotgunHaplotyper thunder/ShotgunManners
THUNDERHDR = $(THUNDERBASE:=.h)
THUNDERSRC = $(THUNDERBASE:=.cpp) thunder/Main.cpp
THUNDEROBJ = $(THUNDERSRC:.cpp=.o)

# Utility Library File Set 
LIBFILE = libsrc/lib-goncalo.a 
LIBMAIN = libsrc/BasicHash libsrc/Error libsrc/FortranFormat \
 libsrc/IntArray libsrc/InputFile \
 libsrc/GenotypeLists libsrc/Hash \
 libsrc/LongArray libsrc/LongHash libsrc/LongLongCounter \
 libsrc/Kinship libsrc/KinshipX libsrc/MapFunction \
 libsrc/MemoryAllocators libsrc/MemoryInfo \
 libsrc/MathMatrix libsrc/MathStats \
 libsrc/MathVector libsrc/MiniDeflate \
 libsrc/Parameters libsrc/Pedigree libsrc/PedigreeAlleleFreq \
 libsrc/PedigreeDescription libsrc/PedigreeFamily libsrc/PedigreeGlobals \
 libsrc/PedigreePerson libsrc/QuickIndex libsrc/Random libsrc/Sort \
 libsrc/StringArray libsrc/StringBasics libsrc/StringMap \
 libsrc/StringHash libsrc/TraitTransformations
LIBPED = libsrc/PedigreeLoader libsrc/PedigreeTwin libsrc/PedigreeTrim
LIBSRC = $(LIBMAIN:=.cpp) $(LIBPED:=.cpp)
LIBHDR = $(LIBMAIN:=.h) libsrc/Constant.h \
 libsrc/MathConstant.h libsrc/PedigreeAlleles.h libsrc/LongInt.h
LIBOBJ = $(LIBSRC:.cpp=.o)
 
# private parameters 
FETCHDIR=$(HOME)/code
DISTRIBDIR=$(HOME)/code/distrib/mach-$(VERSION)

# helpful screen listing available options 
help :
	@echo "MACH Source Distribution"
	@echo " "
	@echo "This Makefile will compile and install MaCH 1.0 on your system"
	@echo " "
	@echo "  Type...          To..."
	@echo "  make help        Display this help screen"
	@echo "  make all         Compile mach1 and related tools"
	@echo "  make install     Install binaries in $(INSTALLDIR)"
	@echo "  make install INSTALLDIR=directory_for_binaries"
	@echo "                   Install binaries in directory_for_binaries"
	@echo "  make clean       Delete temporary files"

# make everything 
all : $(EXECUTABLES)

$(EXECUTABLES) : $(BINDIR)

# dependencies for executables 
$(MACH) : $(LIBFILE) $(MACHOBJ) $(BINDIR)
	$(CXX) $(CFLAGS) -o $@ $(MACHOBJ) $(LIBFILE) -lm -lz

$(THUNDER) : $(LIBFILE) $(THUNDEROBJ) $(BINDIR)
	$(CXX) $(CFLAGS) -o $@ $(THUNDEROBJ) $(LIBFILE) -lm -lz

$(BINDIR) :
	mkdir $(BINDIR)

$(LIBFILE) : $(LIBOBJ) $(LIBHDR)
	ar -cr $@ $(LIBOBJ)
	ranlib $@

$(MACHOBJ) : $(MACHHDR) $(LIBHDR)

$(THUNDEROBJ) : $(THUNDERHDR) $(LIBHDR)

$(CLUSTEROBJ) : $(CLUSTERHDR) $(LIBHDR)

$(LIBOBJ) : $(LIBHDR)

clean :
	-rm -f */*.a */*.o $(EXECUTABLES)

install : all $(INSTALLDIR)
	@echo " "
	@echo Installing to directory $(INSTALLDIR)
	@echo To select a different directory, run
	@echo " "
	@echo make install INSTALLDIR=your_preferred_dir
	@echo " "
	cp $(EXECUTABLES) $(INSTALLDIR)

$(INSTALLDIR) :
	@echo " "
	@echo Creating directory $(INSTALLDIR)
	@echo " "
	@mkdir $(INSTALLDIR)

new-version :
	mkdir -p $(DISTRIBDIR) $(DISTRIBDIR)/mach1 
	mkdir -p $(DISTRIBDIR)/libsrc $(DISTRIBDIR)/thunder
	cp ChangeLog README $(DISTRIBDIR)
	cp Makefile $(DISTRIBDIR)
	cp -R examples $(DISTRIBDIR)
	
fetch :
	cd $(FETCHDIR) ; cp $(MACHSRC) $(MACHHDR) $(DISTRIBDIR)/mach1
	cd $(FETCHDIR) ; cp thunder/ShotgunHaplotyper.cpp thunder/ShotgunManners.cpp thunder/Main.cpp $(DISTRIBDIR)/thunder
	cd $(FETCHDIR) ; cp thunder/ShotgunHaplotyper.h thunder/ShotgunManners.h $(DISTRIBDIR)/thunder
	cd $(FETCHDIR) ; cp $(LIBSRC) $(LIBHDR) $(DISTRIBDIR)/libsrc
	cd $(DISTRIBDIR); csh ../stamp MaCH

.c.o :
	$(CXX) $(CFLAGS) -o $@ -c $*.c

.cpp.X.o :
	$(CXX) $(CFLAGS) -o $@ -c $*.cpp -DVERSION=\"$(VERSION)\" -D__CHROMOSOME_X__

.cpp.o :
	$(CXX) $(CFLAGS) -o $@ -c $*.cpp -DVERSION=\"$(VERSION)\"

archive : clean
	mkdir -p mach-$(VERSION)
	cp -R README Makefile ChangeLog mach-$(VERSION)
	cp -R mach1 libsrc examples thunder mach-$(VERSION)
	tar -cvf mach-$(VERSION).tar mach-$(VERSION)
	gzip -f --best mach-$(VERSION).tar
	rm -rf mach-$(VERSION)

distrib : $(EXECUTABLES)
	mkdir -p mach-$(VERSION)
	cp -R README ChangeLog $(EXECUTABLES) examples mach-$(VERSION)
	tar -cvf `uname`-mach.tar mach-$(VERSION)
	gzip -f `uname`-mach.tar
	rm -rf mach-$(VERSION)

windowszip : $(EXECUTABLES)
	mkdir -p mach-$(VERSION)
	cp -R README ChangeLog $(EXECUTABLES) examples mach-$(VERSION)
	zip -r Windows-mach.zip mach-$(VERSION)
	rm -rf mach-$(VERSION)

.SUFFIXES : .cpp .c .o .X.o $(SUFFIXES)

