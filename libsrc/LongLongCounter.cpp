////////////////////////////////////////////////////////////////////// 
// libsrc/LongLongCounter.cpp 
// (c) 2000-2008 Goncalo Abecasis
// 
// This file is distributed as part of the MaCH source code package   
// and may not be redistributed in any form, without prior written    
// permission from the author. Permission is granted for you to       
// modify this file for your own personal use, but modified versions  
// must retain this copyright notice and must not be distributed.     
// 
// Permission is granted for you to use this file to compile MaCH.    
// 
// All computer programs have bugs. Use this file at your own risk.   
// 
// Saturday April 12, 2008
// 
 
#include "LongLongCounter.h"

LongCounter::LongCounter() : LongHash<int>()
   {
   SetAllowDuplicateKeys(false);
   }

void LongCounter::IncrementCount(long long key)
   {
   int slot = Find(key);

   if (slot == LH_NOTFOUND)
      Add(key, 1);
   else if (Object(slot) == -1)
      Delete(slot);
   else
      Object(slot)++;
   }

void LongCounter::DecrementCount(long long key)
   {
   int slot = Find(key);

   if (slot == LH_NOTFOUND)
      Add(key, -1);
   else if (Object(slot) == 1)
      Delete(slot);
   else
      Object(slot)--;
   }

int LongCounter::GetCount(long long key)
   {
   int slot = Find(key);

   if (slot == LH_NOTFOUND)
      return 0;
   else
      return Object(slot)--;
   }

   
 
