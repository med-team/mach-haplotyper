////////////////////////////////////////////////////////////////////// 
// libsrc/LongLongCounter.h 
// (c) 2000-2008 Goncalo Abecasis
// 
// This file is distributed as part of the MaCH source code package   
// and may not be redistributed in any form, without prior written    
// permission from the author. Permission is granted for you to       
// modify this file for your own personal use, but modified versions  
// must retain this copyright notice and must not be distributed.     
// 
// Permission is granted for you to use this file to compile MaCH.    
// 
// All computer programs have bugs. Use this file at your own risk.   
// 
// Saturday April 12, 2008
// 
 
#ifndef __LONGLONGCOUNTER_H_
#define __LONGLONGCOUNTER_H_

#include "LongHash.h"

class LongCounter : public LongHash<int>
   {
   public:
      LongCounter();

      void IncrementCount(long long key);
      void DecrementCount(long long key);
      int  GetCount(long long key);
   };

#endif


 
